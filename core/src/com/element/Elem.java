package com.element;


import com.badlogic.gdx.Game;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.element.ScreenForGame.Level1;

public class Elem extends Game {
	public SpriteBatch batch;
    public  BitmapFont font;//текст
	public String mapArg;

	public Elem(String arg){
		mapArg = arg;
	}

	
	@Override
	public void create () {
		batch = new SpriteBatch();
        font = new BitmapFont();

		if(mapArg.equals("create")) {
			this.setScreen(new MapCreator(this)); //здесь менять нач. уровень
		}
		else {
			this.setScreen(new Level1(this)); //здесь менять нач. уровень
		}

	}

	@Override
	public void render () {
		super.render();
	}
    @Override
    public void dispose(){
        batch.dispose();
    }
}
