package com.element;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;


import com.element.ScreenForGame.Ground;

/**
 * Created by Nikitka on 22.03.2015.
 */
public class MapCreator implements Screen {

    static final int WORLD_WIDTH = 1600;
    static final int WORLD_HEIGHT = 900;

    int smestY=0;
    int smestX=0;



    Elem game;
    Ground levelground;
    Texture ground[];
    Texture no_go;//крестик




    OrthographicCamera camera;


    int tochX,tochY;
    Short brush; //кисть рисования
    String brushStr;
    Boolean brush_go_no;
    Float brushTime;


    public MapCreator(final Elem gam){
        FileHandle massTexture = Gdx.files.internal("mlink/locs/ground.txt");//загрузка файла
        levelground = new Ground(massTexture.readString()); //создание земли



        game = gam;
        brush = 1000;
        brush_go_no=true;
        brushStr = "1";
        brushTime = 0f;

        levelground.texture = new Texture[15];
        levelground.texture[1] = new Texture(Gdx.files.internal("mlink/ground/001.png"));
        levelground.texture[2] = new Texture(Gdx.files.internal("mlink/ground/002.png"));
        levelground.texture[3] = new Texture(Gdx.files.internal("mlink/ground/003.png"));
        levelground.texture[4] = new Texture(Gdx.files.internal("mlink/ground/004.png"));
        levelground.texture[5] = new Texture(Gdx.files.internal("mlink/ground/005.png"));
        levelground.texture[6] = new Texture(Gdx.files.internal("mlink/ground/006.png"));
        levelground.texture[7] = new Texture(Gdx.files.internal("mlink/ground/007.png"));
        levelground.texture[8] = new Texture(Gdx.files.internal("mlink/ground/008.png"));
        levelground.texture[9] = new Texture(Gdx.files.internal("mlink/ground/009.png"));
        levelground.texture[10] = new Texture(Gdx.files.internal("mlink/ground/010.png"));
        levelground.texture[11] = new Texture(Gdx.files.internal("mlink/ground/011.png"));
        levelground.texture[12] = new Texture(Gdx.files.internal("mlink/ground/012.png"));
        levelground.texture[13] = new Texture(Gdx.files.internal("mlink/ground/013.png"));
        levelground.texture[14] = new Texture(Gdx.files.internal("mlink/ground/014.png"));

        no_go = new Texture(Gdx.files.internal("mlink/ground/drugoe/no_go.png"));

        camera = new OrthographicCamera();
        camera.setToOrtho(false,WORLD_WIDTH,WORLD_HEIGHT);


    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        handleInput();

        camera.update();
        game.batch.setProjectionMatrix(camera.combined);

        game.batch.begin();
       for(int i=0; i<levelground.levely; i++){
            for(int j=0; j<levelground.levelx; j++){
                if (levelground.ground[i][j]!=null) {

                    game.batch.draw(levelground.texture[levelground.ground[i][j]],j*25,i*25);
                    if (levelground.go_no[i][j]==false)
                        game.batch.draw(no_go, j*25,i*25);




                   // game.font.draw(game.batch, textr[i][j], j * 25 + 25, i * 25 + 25);
                }

            }
        }
        game.font.draw(game.batch,"brush = " + brushStr,100+smestX,150-smestY);
        game.batch.end();

        kosanie();
        if (brushTime>0f)
            brushTime-=Gdx.graphics.getDeltaTime();

    }
    public void obnovlenyeTexture(){
        levelground.texture[1] = new Texture(Gdx.files.internal("mlink/ground/001.png"));
        levelground.texture[2] = new Texture(Gdx.files.internal("mlink/ground/002.png"));
        levelground.texture[3] = new Texture(Gdx.files.internal("mlink/ground/003.png"));
        levelground.texture[4] = new Texture(Gdx.files.internal("mlink/ground/004.png"));
        levelground.texture[5] = new Texture(Gdx.files.internal("mlink/ground/005.png"));
        levelground.texture[6] = new Texture(Gdx.files.internal("mlink/ground/006.png"));
        levelground.texture[7] = new Texture(Gdx.files.internal("mlink/ground/007.png"));
        levelground.texture[8] = new Texture(Gdx.files.internal("mlink/ground/008.png"));
        levelground.texture[9] = new Texture(Gdx.files.internal("mlink/ground/009.png"));
        levelground.texture[10] = new Texture(Gdx.files.internal("mlink/ground/010.png"));
        levelground.texture[11] = new Texture(Gdx.files.internal("mlink/ground/011.png"));
        levelground.texture[12] = new Texture(Gdx.files.internal("mlink/ground/012.png"));
        levelground.texture[13] = new Texture(Gdx.files.internal("mlink/ground/013.png"));
        levelground.texture[14] = new Texture(Gdx.files.internal("mlink/ground/014.png"));

    }

    public void zapis(){
        FileHandle massTexture = Gdx.files.local("mlink/locs/ground.txt");
        massTexture.writeString(levelground.zapis(),false);



    }

    public void kosanie(){
        if (Gdx.input.isTouched()) {




            //попытка покраски номер два (04.05.2015)
            tochX = (int)Gdx.input.getX();
            tochY = (int)Gdx.input.getY();




            if      ((WORLD_HEIGHT-tochY-smestY>0)&
                    (tochX+smestX>0)&
                    ((WORLD_HEIGHT-tochY-smestY)/25<levelground.levely)&
                    ((tochX+smestX)/25<levelground.levelx)) {
                if (brush != 1000 & brush<levelground.texture.length)
                levelground.ground[(WORLD_HEIGHT - tochY - smestY) / 25][(tochX + smestX) / 25] = brush;
                levelground.go_no[(WORLD_HEIGHT - tochY - smestY) / 25][(tochX + smestX) / 25] = brush_go_no;
            }
        }
        //кисти земли
        if (Gdx.input.isKeyPressed(Input.Keys.S)){
            zapis();
        }
        if (brushTime<=0) {
            if (Gdx.input.isKeyPressed(Input.Keys.NUM_1)) {
                //brush = 1;
                if (brushStr.length() == 3)
                    brushStr = "";
                brushStr += "1";
                brush = Short.parseShort(brushStr);
                brushTime = 0.2f;


            }
            if (Gdx.input.isKeyPressed(Input.Keys.NUM_2)) {
                //brush = 2;
                if (brushStr.length() == 3)
                    brushStr = "";
                brushStr += "2";
                brush = Short.parseShort(brushStr);
                brushTime = 0.2f;
            }
            if (Gdx.input.isKeyPressed(Input.Keys.NUM_3)) {
                //brush = 3;
                if (brushStr.length() == 3)
                    brushStr = "";
                brushStr += "3";
                brush = Short.parseShort(brushStr);
                brushTime = 0.2f;
            }
            if (Gdx.input.isKeyPressed(Input.Keys.NUM_4)) {
                brush = 4;
                if (brushStr.length() == 3)
                    brushStr = "";
                brushStr += "4";
                brush = Short.parseShort(brushStr);
                brushTime = 0.2f;
            }
            if (Gdx.input.isKeyPressed(Input.Keys.NUM_5)) {
                brush = 5;
                if (brushStr.length() == 3)
                    brushStr = "";
                brushStr += "5";
                brush = Short.parseShort(brushStr);
                brushTime = 0.2f;
            }
            if (Gdx.input.isKeyPressed(Input.Keys.NUM_6)) {
                brush = 6;
                if (brushStr.length() == 3)
                    brushStr = "";
                brushStr += "6";
                brush = Short.parseShort(brushStr);
                brushTime = 0.2f;
            }
            if (Gdx.input.isKeyPressed(Input.Keys.NUM_7)) {
                brush = 7;
                if (brushStr.length() == 3)
                    brushStr = "";
                brushStr += "7";
                brush = Short.parseShort(brushStr);
                brushTime = 0.2f;
            }
            if (Gdx.input.isKeyPressed(Input.Keys.NUM_8)) {
                brush = 8;
                if (brushStr.length() == 3)
                    brushStr = "";
                brushStr += "8";
                brush = Short.parseShort(brushStr);
                brushTime = 0.2f;
            }
            if (Gdx.input.isKeyPressed(Input.Keys.NUM_9)) {
                brush = 9;
                if (brushStr.length() == 3)
                    brushStr = "";
                brushStr += "9";
                brush = Short.parseShort(brushStr);
                brushTime = 0.2f;
            }
            if (Gdx.input.isKeyPressed(Input.Keys.NUM_0)) {
                brush = 10;
                if (brushStr.length() == 3)
                    brushStr = "";
                brushStr += "0";
                brush = Short.parseShort(brushStr);
                if (brush == 0){ brush = 1; brushStr = "1";}
                brushTime = 0.2f;
            }




        }



        //кисть прохода
        if (Gdx.input.isKeyPressed(Input.Keys.Z)){
            brush_go_no=true;
        brush = 1000;}

        if (Gdx.input.isKeyPressed(Input.Keys.X)){
            brush_go_no=false;
        brush = 1000;}




        //обновление текстур
        if (Gdx.input.isKeyPressed(Input.Keys.O)){
            obnovlenyeTexture();
        }

    }

    public void handleInput(){
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            if (smestX>0){
            camera.translate(-6, 0, 0);
            smestX=smestX-6;}
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            if (smestX<1700){
            camera.translate(6, 0, 0);
            smestX=smestX+6;}
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            if (smestY<0){
            camera.translate(0, -6, 0);
            smestY=smestY+6;}
        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            if (smestY>-1900){
            camera.translate(0, 6, 0);
            smestY=smestY-6;}
        }

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
dispose();
    }

    @Override
    public void dispose() {
        for (int i=1; i<10;i++){

            levelground.texture[i].dispose();
        }

    }
}
