package com.element.Others;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.element.Elem;

/**
 * Created by Nikitka on 20.07.2015.
 */
public class Interface {
    Elem game;
    Texture Menu;
    float menu_x, menu_y;
    MyButton ResumeBtn;
    MyButton TalentBtn;
    MyButton SettingsBtn;
    MyButton MapBtn;
    MyButton ExitBtn;
    MyButton PauseBtn;
    int WORLD_HEIGHT;
    int WORLD_WIDTH;

    Texture HPplayerDown, HPplayerUp;

    public Interface(Elem gam,int WIDTH,int HEIGHT ){
        WORLD_HEIGHT = HEIGHT;
        WORLD_WIDTH = WIDTH;
        game = gam;
        Menu = new Texture(Gdx.files.internal("mlink/others/menu.png"));
        menu_x = (WORLD_WIDTH - (600*WORLD_HEIGHT/800))/2;
        menu_y = 0;

        ResumeBtn = new MyButton(new Texture(Gdx.files.internal("mlink/others/menu_resume.png")),(WORLD_WIDTH-340*WORLD_HEIGHT/800)/2,WORLD_HEIGHT-(219*WORLD_HEIGHT/800),340*WORLD_HEIGHT/800,79*WORLD_HEIGHT/800);
        TalentBtn = new MyButton(new Texture(Gdx.files.internal("mlink/others/menu_talents.png")),(WORLD_WIDTH-340*WORLD_HEIGHT/800)/2,WORLD_HEIGHT-(357*WORLD_HEIGHT/800),340*WORLD_HEIGHT/800,79*WORLD_HEIGHT/800);
        SettingsBtn = new MyButton(new Texture(Gdx.files.internal("mlink/others/menu_settings.png")),(WORLD_WIDTH-340*WORLD_HEIGHT/800)/2,WORLD_HEIGHT-(495*WORLD_HEIGHT/800),340*WORLD_HEIGHT/800,79*WORLD_HEIGHT/800);
        MapBtn = new MyButton(new Texture(Gdx.files.internal("mlink/others/menu_map.png")),(WORLD_WIDTH-340*WORLD_HEIGHT/800)/2,WORLD_HEIGHT-(633*WORLD_HEIGHT/800),340*WORLD_HEIGHT/800,79*WORLD_HEIGHT/800);
        ExitBtn = new MyButton(new Texture(Gdx.files.internal("mlink/others/menu_exit.png")),(WORLD_WIDTH-340*WORLD_HEIGHT/800)/2,WORLD_HEIGHT-(771*WORLD_HEIGHT/800),340*WORLD_HEIGHT/800,79*WORLD_HEIGHT/800);
        PauseBtn = new MyButton(new Texture(Gdx.files.internal("mlink/others/pause.png")),1200-32,675-32,32,32);

        HPplayerDown = new Texture(Gdx.files.internal("mlink/others/hptext.png"));
        HPplayerUp = new Texture(Gdx.files.internal("mlink/others/hptext2.png"));

    }

    public void render(Boolean pause, float smestX, float smestY, float PlayerHP){
         if(!pause) {
             game.batch.draw(HPplayerDown, smestX + 10, WORLD_HEIGHT + smestY - 40, 200, 20);
             game.batch.draw(HPplayerUp, smestX + 10, WORLD_HEIGHT + smestY - 40, 200 * PlayerHP / 100, 20);//200-длина полосы, 100- максимальное здоровье перса
             game.font.draw(game.batch, "HP = " + Integer.toString((int) PlayerHP), 210 + smestX, smestY + WORLD_HEIGHT - 20);

             game.batch.draw(PauseBtn.texture, PauseBtn.posX + smestX, PauseBtn.posY + smestY);
         }
        if(pause){
            game.batch.begin();
            game.batch.draw(Menu,menu_x+smestX,menu_y+smestY,675f/800f*600f,675f);
            ResumeBtn.render(game,smestX,smestY);
            TalentBtn.render(game,smestX,smestY);
            SettingsBtn.render(game,smestX,smestY);
            MapBtn.render(game,smestX,smestY);
            ExitBtn.render(game,smestX,smestY);

            game.batch.end();
        }
    }

    public Boolean cosanie(Boolean pause,float pizdetsX,float pizdetsY,float smestX, float smestY) {
        if (!pause) {
            if ((pizdetsX >= PauseBtn.posX + smestX) & (pizdetsY >= PauseBtn.posY + smestY) & (pizdetsX <= PauseBtn.posX + smestX + PauseBtn.width) & (pizdetsY <= PauseBtn.posY + smestY + PauseBtn.height))
                return true;

        }
        if (pause) {
            if (ResumeBtn.cosanie(pizdetsX, pizdetsY, smestX, smestY))
                return false;
            if (ExitBtn.cosanie(pizdetsX,pizdetsY,smestX,smestY))
                System.exit(0);



        }
        return pause;
    }

}

