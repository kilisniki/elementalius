package com.element.Others;

import com.badlogic.gdx.graphics.Texture;
import com.element.Elem;

/**
 * Created by Nikitka on 05.07.2015.
 */
public class MyButton {
    public Texture texture;
    public float posX;
    public float posY;
    public float width;
    public float height;
    Boolean touchable;



    public MyButton(Texture text, float X, float Y,float W, float H){
        texture = text;
        posX = X;
        posY = Y;
        width = W;
        height = H;
    }
    public void render(Elem gam,float smX,float smY){
        gam.batch.draw(texture,posX+smX,posY+smY,width,height);

    }

    public Boolean cosanie(float pizX, float pizY, float smX, float smY){
        if ((pizX>=posX+smX)&(pizY>=posY+smY)&(pizX<=posX+width+smX)&(pizY<=posY+height+smY))

        return true;

        return false;
    }



}
