package com.element.Skills;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.element.Elem;
import com.element.ScreenForGame.Ground;

import java.util.ArrayList;

/**
 * Created by Nikitka on 25.05.2015.
 */
public class FirePool extends Skill {


    public FirePool(float x, float y){
        posX = x;
        posY = y;
        time = 5f;
        damage = 10f;
          texture = new ArrayList<Texture>();
          texture.add(new Texture(Gdx.files.internal("mlink/Skills/fire.png")));
          texture.add(new Texture(Gdx.files.internal("mlink/Skills/fire2.png")));
          texture.add(new Texture(Gdx.files.internal("mlink/Skills/fire.png")));
          texture.add(new Texture(Gdx.files.internal("mlink/Skills/fire3.png")));
          timer = 0f;
          nuberTexture = 0;
        //texture = new Texture(Gdx.files.internal("mlink/Skills/fire.png"));

        squareX = (int)posX / 25;
        squareY = (int)posY / 25;

    }

    public void act(Ground ground, float delta) throws ArrayIndexOutOfBoundsException{

       try {
           if (ground.mobs[squareY][squareX] != null) {
               mob = ground.mobs[squareY][squareX];
               mob.hitPoint -= damage * mob.resistFire * delta;
           }
       }catch (ArrayIndexOutOfBoundsException e){}
       try {
           if (ground.mobs[squareY - 1][squareX] != null) {
               mob = ground.mobs[squareY - 1][squareX];
               mob.hitPoint -= damage * mob.resistFire * delta;
           }
       }catch (ArrayIndexOutOfBoundsException e){}
       try {
           if (ground.mobs[squareY + 1][squareX] != null) {
               mob = ground.mobs[squareY + 1][squareX];
               mob.hitPoint -= damage * mob.resistFire * delta;
           }
       }catch (ArrayIndexOutOfBoundsException e){}

       try{
         if (ground.mobs[squareY][squareX-1]!=null) {
             mob = ground.mobs[squareY][squareX - 1];
             mob.hitPoint -= damage * mob.resistFire * delta;
         }
       }catch (ArrayIndexOutOfBoundsException e){}
       try {
           if (ground.mobs[squareY][squareX + 1] != null) {
               mob = ground.mobs[squareY][squareX + 1];
               mob.hitPoint -= damage * mob.resistFire * delta;
           }
       }catch (ArrayIndexOutOfBoundsException e){}

        time-=delta;


        timer+=delta;
        if (timer>=0.1f){
            nuberTexture = (nuberTexture + 1)%4;
            timer = 0f;  }

    }
    public void render(Elem gam){

        gam.batch.draw(texture.get(nuberTexture),squareX*25,squareY*25);
        gam.batch.draw(texture.get(nuberTexture),(squareX-1)*25,squareY*25);
        gam.batch.draw(texture.get(nuberTexture),(squareX+1)*25,squareY*25);
        gam.batch.draw(texture.get(nuberTexture),squareX*25,(squareY-1)*25);
        gam.batch.draw(texture.get(nuberTexture),squareX*25,(squareY+1)*25);


        /*gam.batch.draw(texture,squareX*25,squareY*25);
        gam.batch.draw(texture,(squareX-1)*25,squareY*25);
        gam.batch.draw(texture,(squareX+1)*25,squareY*25);
        gam.batch.draw(texture,squareX*25,(squareY-1)*25);
        gam.batch.draw(texture,squareX*25,(squareY+1)*25);
*/

    }
}
