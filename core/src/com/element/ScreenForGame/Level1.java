package com.element.ScreenForGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;


import com.element.Elem;
import com.element.Mobs.Enemy.Skeleton;

import com.element.Others.Interface;

import com.element.Player;
import com.element.Skills.FirePool;
import com.element.Skills.Skill;

import java.util.ArrayList;


/**
 * Created by Nikitka on 28.03.2015.
 */
public class Level1 implements Screen {
    Elem game;
Texture no_go;
    static final int WORLD_WIDTH = 1200;
    static final int WORLD_HEIGHT = 675;

    float smestY=0;
    float smestX=0;

    //int MyX=0;
   // int MyY=0;
    int SchetchikSkelet;


    float pizdetsX;
    float pizdetsY;

    Ground levelground;




    Interface MyInterface;


    Boolean pause=false;




    OrthographicCamera camera;




    Player player;
    Skeleton skelet;
    ArrayList<Skeleton> agroMobs;

    float CreateSkeletonTimer =0;


    ArrayList<Skill> skil;








    public Level1(final Elem gam){
        game=gam;
        MyInterface = new Interface(game,WORLD_WIDTH,WORLD_HEIGHT);
        FileHandle massTexture = Gdx.files.internal("mlink/locs/ground.txt");//загрузка файла //android/assets/


        levelground = new Ground(massTexture.readString()); //создание земли
        no_go = new Texture(Gdx.files.internal("mlink/ground/drugoe/no_go.png"));


        levelground.texture = new Texture[15];
        levelground.texture[1] = new Texture(Gdx.files.internal("mlink/ground/001.png"));
        levelground.texture[2] = new Texture(Gdx.files.internal("mlink/ground/002.png"));
        levelground.texture[3] = new Texture(Gdx.files.internal("mlink/ground/003.png"));
        levelground.texture[4] = new Texture(Gdx.files.internal("mlink/ground/004.png"));
        levelground.texture[5] = new Texture(Gdx.files.internal("mlink/ground/005.png"));
        levelground.texture[6] = new Texture(Gdx.files.internal("mlink/ground/006.png"));
        levelground.texture[7] = new Texture(Gdx.files.internal("mlink/ground/007.png"));
        levelground.texture[8] = new Texture(Gdx.files.internal("mlink/ground/008.png"));
        levelground.texture[9] = new Texture(Gdx.files.internal("mlink/ground/009.png"));
        levelground.texture[10] = new Texture(Gdx.files.internal("mlink/ground/010.png"));
        levelground.texture[11] = new Texture(Gdx.files.internal("mlink/ground/011.png"));
        levelground.texture[12] = new Texture(Gdx.files.internal("mlink/ground/012.png"));
        levelground.texture[13] = new Texture(Gdx.files.internal("mlink/ground/013.png"));
        levelground.texture[14] = new Texture(Gdx.files.internal("mlink/ground/014.png"));


        camera = new OrthographicCamera();
        camera.setToOrtho(false,WORLD_WIDTH,WORLD_HEIGHT);

        player = new Player(levelground);
        player.posX=30;
        player.posY=30;



        //создание моих подопытных
        agroMobs = new ArrayList<Skeleton>();
        for (int i=1; i<=3; i++){
           skelet = new Skeleton(1,300,i*200,levelground);
            agroMobs.add(skelet);
        }
      /*  Random random = new Random();

        for (int i=1; i<=100; i++){
          skelet = new Skeleton(1,random.nextInt(100*25-100)+50, random.nextInt(100*25-100), levelground);
          agroMobs.add(skelet);
        }
        SchetchikSkelet = 103; */

        skil = new ArrayList<Skill>();





        pause = false;










    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

        if (pause==false){

        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);




        camera.update();
        game.batch.setProjectionMatrix(camera.combined);


        //!!!НАЧАЛО ОТРИСОВКИ!!!
        game.batch.begin();

        for (int i = 0; i < levelground.levely; i++) {
            for (int j = 0; j < levelground.levelx; j++) {
                if (levelground.ground[i][j] != null) {

                    game.batch.draw(levelground.texture[levelground.ground[i][j]], j * 25, i * 25);
                   //  if (levelground.go_no[i][j]==false)
                    // game.batch.draw(no_go, j*25,i*25);  //зарисовка клеток в которые нельзя пройти
                    // game.font.draw(game.batch, textr[i][j], j * 25 + 25, i * 25 + 25);//отрисовка номера клетки
                }
            }
        }
        //отрисовка скилов

        for (int i = 0; i < skil.size(); i++) {
            if (skil.get(i) != null) {
                skil.get(i).render(game);
            }
        }



            game.font.draw(game.batch, "Skelets= " + Integer.toString(SchetchikSkelet), 25 + smestX, smestY + 50);
            game.font.draw(game.batch, "FPS = " + Integer.toString((int) (1 / Gdx.graphics.getDeltaTime())), 75 + smestX, smestY + 95);

        //отрисовка плохих мобов

        for (int i = 0; i < agroMobs.size(); i++) {
            agroMobs.get(i).draw(game.batch, 0);
            if (!agroMobs.get(i).die)
                game.font.draw(game.batch, Integer.toString((int) agroMobs.get(i).hitPoint), agroMobs.get(i).posX, agroMobs.get(i).posY);
        }


        //ОТРИСОВКА ИНТЕРФЕЙСА
       MyInterface.render(pause,smestX,smestY,player.hitPoint);

        player.draw(game.batch, 0);


        game.batch.end();
        //!!!КОНЕЦ ОТРИСОВКИ!!!


        //ДЕЙСТВИЯ
        //что делать мобу?
        for (int i = 0; i < agroMobs.size(); i++) {
            if (agroMobs.get(i).hitPoint < 1)
                agroMobs.get(i).died();
            if (agroMobs.get(i).target != null)
                agroMobs.get(i).act(Gdx.graphics.getDeltaTime());
            else if (!player.die)
                agroMobs.get(i).agro(player);//поставить на проверку массив с хорошими мобами
        }

        if (CreateSkeletonTimer > 0) CreateSkeletonTimer -= Gdx.graphics.getDeltaTime();


        if (player.hitPoint <= 1 & !player.die) {
            player.died();
            for (int i = 0; i < agroMobs.size(); i++) {
                agroMobs.get(i).target = null;
            }
        }


        //скилы
        for (int i = 0; i < skil.size(); i++) {
            if (skil.get(i) != null) {
                skil.get(i).act(levelground, Gdx.graphics.getDeltaTime());
                if (skil.get(i).time <= 0) {
                    skil.get(i).delete();
                    skil.remove(i);
                }
            }
        }

            if (!player.die)
                handleInput();

    }









//ОТРИСОВКА ПРИ ПАУЗЕ
        if (pause){
            MyInterface.render(pause,smestX,smestY,player.hitPoint);
            pauseinput();

        }






    }

    @Override
    public void resize(int width, int height) {
    }




    public void handleInput(){
        //создание скелета или удаление
        if(Gdx.input.isTouched() & CreateSkeletonTimer<=0){


            pizdetsX = Gdx.input.getX()*3/4+smestX; //это переменные для отслеживания косания. соотношения у каждой стороны может быть различным
            pizdetsY = WORLD_HEIGHT - Gdx.input.getY()*3/4 + smestY;  //3\4 - это соотношение отображения и окна приложения(нужно заменить на  a и b)

            //проверка на нажатие кнопок

            if(pause != MyInterface.cosanie(pause,pizdetsX,pizdetsY,smestX,smestY))
                pause = true;
            else
                skil.add(new FirePool(pizdetsX,pizdetsY));



            CreateSkeletonTimer = 2f;
            }


            //переменные для отслеживания косания относительно окна
            //MyX = Gdx.input.getX();
            //MyY = Gdx.input.getY();




        //МУКИ ГОРЕ И СТРАДАНИЕ ТОМУ КТО РЕШИТ ПЕРЕПИСАТЬ ЭТО НА СВОЙ ЛАД ИЛИ ХОТЯ БЫ РАЗОБРАТЬСЯ В ЭТОМ АДУ........................................................
        //.................
        //..................
        //......................
        boolean a = false; //переменная для ослеживания нажатий комб клавиш
        //это начало первых двух кнопок
        if ((Gdx.input.isKeyPressed(Input.Keys.D))&((Gdx.input.isKeyPressed(Input.Keys.W))|(Gdx.input.isKeyPressed(Input.Keys.S)))){
            a = true;
            if (levelground.go_no[(int)(player.posY/25)][(int)((player.posX+25+player.velocity/1.4)/25)]==true) {
                if ((smestX < levelground.levelx * 25 - WORLD_WIDTH) & (player.posX - smestX >= WORLD_WIDTH / 3 * 2)) {
                    camera.translate(player.velocity / 1.4f, 0, 0);
                    smestX += player.velocity / 1.4;
                }
                if (player.posX < levelground.levelx * 25 - 37)
                    player.posX += player.velocity / 1.4; //где 37- ширина картинки персонажа, а 25 размеры текстуры блока земли
            }
            if (Gdx.input.isKeyPressed(Input.Keys.W)) {
                if (levelground.go_no[(int)((player.posY+15+player.velocity/1.4)/25)][(int)player.posX/25]==true){
                if ((smestY < levelground.levely*25-WORLD_HEIGHT) & (player.posY - smestY >= WORLD_HEIGHT / 3 * 2)) {
                    camera.translate(0, player.velocity/1.4f, 0);
                    smestY +=player.velocity/1.4;
                }
                if (player.posY < levelground.levely * 25 - 49) {player.posY += player.velocity/1.4;} //где 49 - высота картинки персонажа
            }}
            if (Gdx.input.isKeyPressed(Input.Keys.S)) {
                if (levelground.go_no[(int)((player.posY-player.velocity/1.4)/25)][(int)((player.posX+25)/25)]==true){
                if ((smestY>0)&(player.posY-smestY<=WORLD_HEIGHT/3)){
                    camera.translate(0, -player.velocity/1.4f, 0);
                    smestY-=player.velocity/1.4;
                }
                if ((player.posY>0)){player.posY-=player.velocity/1.4;}
            }}
         }// это конец двух кнопок

         //это начало вторых двух кнопок
        if ((Gdx.input.isKeyPressed(Input.Keys.A))&((Gdx.input.isKeyPressed(Input.Keys.W))|(Gdx.input.isKeyPressed(Input.Keys.S)))&(a==false)){
            a=true;
            if (levelground.go_no[(int)(player.posY/25)][(int)((player.posX-player.velocity/1.4)/25)]==true) {
                if ((smestX > 0) & (player.posX - smestX <= WORLD_WIDTH / 3)) {
                    camera.translate(-player.velocity / 1.4f, 0, 0);
                    smestX -= player.velocity / 1.4;
                }
                if (player.posX > 0) player.posX -= player.velocity / 1.4;
            }
            if (Gdx.input.isKeyPressed(Input.Keys.W)) {
                if (levelground.go_no[(int)((player.posY+15+player.velocity/1.4)/25)][(int)player.posX/25]==true){
                if ((smestY < levelground.levely*25-WORLD_HEIGHT) & (player.posY - smestY >= WORLD_HEIGHT / 3 * 2)) {
                    camera.translate(0, player.velocity/1.4f, 0);
                    smestY +=player.velocity/1.4;
                }
                if (player.posY < levelground.levely * 25 - 49) {player.posY += player.velocity/1.4;} //где 49 - высота картинки персонажа
            }
            }
            if (Gdx.input.isKeyPressed(Input.Keys.S)) {
                if (levelground.go_no[(int)((player.posY-player.velocity/1.4)/25)][(int)((player.posX+25)/25)]==true){
                    if ((smestY > 0) & (player.posY - smestY <= WORLD_HEIGHT / 3)) {
                        camera.translate(0, -player.velocity / 1.4f, 0);
                        smestY -= player.velocity / 1.4;
                    }
                    if ((player.posY > 0)) {
                        player.posY -= player.velocity / 1.4;
                    }
                }
            }
        }// это конец двух кнопок

        if (Gdx.input.isKeyPressed(Input.Keys.A)&a==false) {
            if (levelground.go_no[(int)(player.posY/25)][(int)((player.posX-player.velocity)/25)]==true) {
            if ((smestX>0)&(player.posX-smestX<=WORLD_WIDTH/3)){
                camera.translate(-player.velocity, 0, 0);
                smestX=smestX-player.velocity;}
            if (player.posX>0)player.posX=player.posX-player.velocity;
        }}
        if (Gdx.input.isKeyPressed(Input.Keys.D)&a==false) {
            if (levelground.go_no[(int)(player.posY/25)][(int)((player.posX+25+player.velocity)/25)]==true) {
            if ((smestX<levelground.levelx*25-WORLD_WIDTH)&(player.posX-smestX>=WORLD_WIDTH/3*2)){
                camera.translate(player.velocity, 0, 0);
                smestX=smestX+player.velocity;}
            if (player.posX<levelground.levelx*25-37)player.posX+=player.velocity;//где 37- ширина картинки персонажа, а 25 размеры текстуры блока земли
        }}
        if (Gdx.input.isKeyPressed(Input.Keys.S)&a==false) {
            if (levelground.go_no[(int)((player.posY-player.velocity)/25)][(int)((player.posX+25)/25)]==true){
            if ((smestY>0)&(player.posY-smestY<=WORLD_HEIGHT/3)){
                camera.translate(0, -player.velocity, 0);
                smestY=smestY-player.velocity;
                }
            if ((player.posY>0)){player.posY-=player.velocity;}
        }}
        if (Gdx.input.isKeyPressed(Input.Keys.W)&a==false) {
            if (levelground.go_no[(int)((player.posY+15+player.velocity)/25)][(int)(player.posX+15)/25]==true){
            if ((smestY<levelground.levely*25-WORLD_HEIGHT)&(player.posY-smestY>=WORLD_HEIGHT/3*2)){
                camera.translate(0, player.velocity, 0);
                smestY=smestY+player.velocity;
                }
            if (player.posY<levelground.levely*25-49){player.posY+=player.velocity;} //где 49 - высота картинки персонажа
        }}

    }

    public void pauseinput(){
        if(Gdx.input.isTouched()) {
            pizdetsX = Gdx.input.getX() * 3 / 4 + smestX; //это переменные для отслеживания косания. соотношения у каждой стороны может быть различным
            pizdetsY = WORLD_HEIGHT - Gdx.input.getY() * 3 / 4 + smestY;  //3\4 - это соотношение отображения и окна приложения(нужно заменить на  a и b)

            //проверка на нажатие кнопок
            pause = MyInterface.cosanie(pause,pizdetsX,pizdetsY,smestX,smestY);
           /* if (ResumeBtn.cosanie(pizdetsX, pizdetsY, smestX, smestY)) {
                pause = false;
            }*/
        }

    }

    @Override
    public void pause() {
        System.exit(0);

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        dispose();

    }

    @Override
    public void dispose() {
        for (int i=1; i<10;i++){

            levelground.texture[i].dispose();
        }

    }


}
