package com.element.ScreenForGame;

import com.badlogic.gdx.graphics.Texture;
import com.element.Mobs.MyMob;

/**
 * Created by Nikitka on 10.04.2015.
 */
public class Ground {
    public Short[][] ground;
    public Texture[] texture;
    public Boolean[][] go_no; //массив прохождения
    public String readString;
    public MyMob[][] mobs;

    public int levelx, levely;

    public Ground(String string){
        readString = string;
        levelx = Integer.parseInt(readString.substring(0,3));//первые три цыфры размер по х
        levely = Integer.parseInt(readString.substring(3,6));//вторые три цифры размер по y
        ground=new Short[levely][levelx];//создание массива для хранения значения ячеек
        //считывание значения ячеек
        for(int i=1; i<=levely; i++){
            for(int j=1; j<=levelx; j++){

                ground[i-1][j-1]=Short.valueOf(readString.substring(6+((i-1)*levelx*3+(j-1)*3),6+((i-1)*levelx*3+(j-1)*3)+3));
            }
        }
        //УЗНАЕМ ВОЗМОЖНОСТЬ ПРОХОЖДЕНИЯ ДЛЯ КАЖДОЙ КЛЕТКИ
        readString = readString.substring(levelx*levely*3+6);
        go_no = new Boolean[levely][levelx];
        //считывание значения ячеек
        for(int i=0; i<levely; i++){
            for(int j=0; j<levelx; j++){

                if (readString.substring(j+i*levelx,j+i*levelx+1).equals("0"))
                    go_no[i][j]=false; //прохода нет
                else
                    go_no[i][j]=true; //проход есть
            }

        }
        mobs = new MyMob[levely][levelx];


    }

    public String zapis(){
        String stroka_bool="";//чтобы не проводить цикл два раза эта строка будет отвечать за файл прохода
        readString = Integer.toString(levelx)+Integer.toString(levely);//размер карты
        for(int i=0;i<levely;i++){ //100 * 100 размер данной карты
            for (int j=0;j<levelx;j++){
                if (ground[i][j]!=null)
                    readString = readString+ String.valueOf(ground[i][j]/100)  + String.valueOf(ground[i][j]/10) + String.valueOf(ground[i][j]%10);
                if (ground[i][j]==null)
                    readString = readString + "000";

                if (go_no[i][j]==true)
                    stroka_bool+="1";
                else
                    stroka_bool+="0";

            }
        }
        return (readString + stroka_bool + "end");
    }

}
