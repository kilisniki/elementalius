package com.element.Mobs;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.element.ScreenForGame.Ground;

/**
 * Created by Nikitka on 31.03.2015.
 */
public class MyMob extends Actor {
   public float posX;
    public float posY;
    public float hitPoint;
    public int damage;
    public int defense;
    public int level;
    public boolean agro;
    public boolean die;
    public Texture texture;
    public Texture dieTexture;
    public float velocity;
    public float velocityY, velocityX;
    public Ground ground;

    public float pox,poy;
    public int px,py,px2,py2;


    public float resistFire;

    public void draw(SpriteBatch batch, float qwerty){
        batch.draw(texture, posX,posY);
    }
    public void died(){
        ground.go_no[(int)posY/25][(int)posX/25]=true;
        ground.mobs[(int)posY/25][(int)posX/25]=null;

        texture.dispose();
        texture = new Texture("mlink/Mobs/diedMob.png");
        die = true;


    }




}
