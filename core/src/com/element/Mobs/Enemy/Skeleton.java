package com.element.Mobs.Enemy;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

import com.element.Mobs.MyMob;
import com.element.ScreenForGame.Ground;

/**
 * Created by Nikitka on 31.03.2015.
 */
public class Skeleton extends MyMob {


   public MyMob target;

    public Skeleton(int lv, float x, float y, Ground gr){
        level = lv;//
        posX = x;
        posY = y;
        texture = new Texture(Gdx.files.internal("mlink/Mobs/skelet.png"));
        hitPoint = 100;//
        damage=10;//
        velocity =5*10;
        ground = gr;
        die = false;
        resistFire = 1;
        gr.mobs[(int)posY/25][(int)posX/25]=this;
        gr.go_no[(int)posY/25][(int)posX/25]=false;
    }

    public void act(float delta){
        if (!die){

        if ((((posX-target.posX<32)&(posX-target.posX>0))|((target.posX-posX<15)&(target.posX-posX>0)))
                &
                (((posY-target.posY<32)&(posY-target.posY>0))|((target.posY-posY<15)&(target.posY-posY>0)))
                ){
            atack(delta);
        }else {
            double x,y,z;

            x = target.posX - posX;//!
            y = target.posY - posY;//!
            z = Math.sqrt(x*x + y*y);
            velocityX = (float)(velocity * x/z);
            velocityY = (float)(velocity * y/z);




            px = (int)posX;
            py = (int)posY;

            px=px/25;
            py=py/25;


            pox = posX+(velocityX*delta);
            poy = posY+(velocityY*delta);

            px2 = (int)pox;
            py2 = (int)poy;

            px2=px2/25;
            py2=py2/25;

            ground.go_no[py][px]=true;
            ground.mobs[py][px]=null;

            if (ground.go_no[py][px2]){
                posX = pox;
                px = px2;
            }
            if (ground.go_no[py2][px]){
                posY = poy;
                py = py2;
            }

            ground.go_no[py][px]=false;
            ground.mobs[py][px]=this;

        }
        }
    }
    public void agro(MyMob mob){
        if ( (posX-mob.posX)*(posX-mob.posX)+(posY-mob.posY)*(posY-mob.posY)<=30000  )
            target = mob;
    }
    public  void atack(float delta){
        target.hitPoint-=damage*delta;

    }


}
