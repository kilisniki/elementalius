package com.element.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.element.Elem;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Elementalius";
		config.width = 1600;//1600
		config.height = 900;//900
		//Gdx.graphics.getWidth(); Gdx.graphics.getHeight();
		//легенда: можно как-то отключить верт.синхр. чтобы картинка не дерагалсь <<неправда << правда, меньше стало дергаться
		// полноэкранный режим
		//config.fullscreen = true;
		// вертикальная синхронизация
		config.vSyncEnabled = true;

		String parg;

		try{
			parg = arg[0];
		}
		catch (ArrayIndexOutOfBoundsException e){
			parg = "0";
		}



		new LwjglApplication(new Elem(parg), config);
	}
}
